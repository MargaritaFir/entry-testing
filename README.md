## Firsova Margarita (Contact information)

  Email: margaret2009@rambler.ru

  Phone +7(981)121-40-92


# Available tasks
Hello, here is testing round tasks for Frontend School in T-Systems.


## 1 Broken page
This nice mosaic was totally broken, please fix it.

[fixed page](broken-page/index.html)

## 2 Maze
Help orange square get out of the frightening maze.

Unfortunately, the orange square doesn't know how to make moves up and left.
You have to teach it to do that.

[Orange square left of the maze](maze/index.html)

## 3 Page from JPEG
We've lost sources of our main page, only one last screenshot was left.

Please help us to compose this web-page again.

[Finished file](page-from-jpeg/index.html)


## 4 Algorithm in JS
Fibonacci has called. Seems he can’t recall his numbers, except for the first two: 1, 1.

Could you create .js file, that Fibonacci can copypaste in his browser console and get any Nth number by inputing the N into it.

[Algorithm Fibonacii](fibonacii/fibonacii.js)

## How to pass
Make a fork from this repository, complete tasks and add your contact info for next communication.

And one more thing: be brave and creative :)
We are waiting for you in our team!
